<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property integer $parent
 * @property string $image
 * @property integer $col_md
 * @property integer $col_lg
 * @property integer $position
 * @property integer $in_new_tab
 * @property integer $pageType
 * @property string $mapImage
 * @property integer $itinerary
 * @property integer $includedFeatures
 * @property integer $datesAndPricing
 * @property integer $hotels
 * @property integer $shipInformation
 * @property integer $gallery
 * @property integer $equipment
 * @property integer $yachtSpecs
 *
 * @property Itinerary[] $itineraries
 * @property Pricing $pricing
 * @property YachtSpecifications $yachtSpecifications
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'parent', 'image', 'position'], 'required'],
            [['title', 'slug', 'content'], 'string'],
            [['parent', 'col_md', 'col_lg', 'position', 'in_new_tab', 'pageType', 'itinerary', 'includedFeatures', 'datesAndPricing', 'hotels', 'shipInformation', 'gallery', 'equipment', 'yachtSpecs'], 'integer'],
            [['image'], 'string', 'max' => 300],
            [['mapImage'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'content' => 'Content',
            'parent' => 'Parent',
            'image' => 'Image',
            'col_md' => 'Col Md',
            'col_lg' => 'Col Lg',
            'position' => 'Position',
            'in_new_tab' => 'In New Tab',
            'pageType' => 'Page Type',
            'mapImage' => 'Map Image',
            'itinerary' => 'Itinerary',
            'includedFeatures' => 'Included Features',
            'datesAndPricing' => 'Dates And Pricing',
            'hotels' => 'Hotels',
            'shipInformation' => 'Ship Information',
            'gallery' => 'Gallery',
            'equipment' => 'Equipment',
            'yachtSpecs' => 'Yacht Specs',
        ];
    }

    public function extraFields() {
        return ['itineraries', 'yachtSpecifications', 'pricing', 'relatedRoutes'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItineraries()
    {
        return $this->hasMany(Itinerary::className(), ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricing()
    {
        return $this->hasMany(Pricing::className(), ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYachtSpecifications()
    {
        return $this->hasOne(YachtSpecifications::className(), ['menu_id' => 'id']);
    }

    /**
     * Gets related routes based on parent
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedRoutes() {
        $slug = Yii::$app->request->get('slug');
        return $this->hasMany(Page::className(), ['parent' => 'parent'])->where('slug != :slug',[':slug' => $slug]);
    }

    /**
     * Returns number of items with inserted parentID
     * @param int $parentID
     * @return int|string
     */
    public function calculatePosition($parentID) {
        return Page::find()->where('parent = :parent',[':parent' => $parentID])->count();
    }

    /**
     * If previous menu item has parent id != 0, then disable further nesting
     * @param int $parentID parentID from current menu item
     * @return bool
     */
    public function getPrevParentID($parentID){
        if ($parentID != 0 && $m = Page::findOne(['id' => $parentID])->parent != 0) {
            return false;
        }
        return true;
    }
}
