<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yacht_specifications".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property string $model
 * @property string $year
 * @property string $length
 * @property string $beam
 * @property string $draught
 * @property string $berths
 * @property string $cabins
 * @property string $wcShower
 * @property string $engine
 * @property string $fuelCapacity
 * @property string $waterCapacity
 *
 * @property Page $menu
 */
class YachtSpecifications extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yacht_specifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id'], 'required'],
            [['menu_id'], 'integer'],
            [['model', 'year', 'length', 'beam', 'draught', 'berths', 'cabins', 'wcShower', 'engine', 'fuelCapacity', 'waterCapacity'], 'string'],
            [['menu_id'], 'unique'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'model' => 'Model',
            'year' => 'Year',
            'length' => 'Length',
            'beam' => 'Beam',
            'draught' => 'Draught',
            'berths' => 'Berths',
            'cabins' => 'Cabins',
            'wcShower' => 'Wc Shower',
            'engine' => 'Engine',
            'fuelCapacity' => 'Fuel Capacity',
            'waterCapacity' => 'Water Capacity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'menu_id']);
    }
}
