<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pricing".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property string $startDate
 * @property string $endDate
 * @property string $perPersonInTwin
 * @property string $netSingleSupplement
 * @property string $supplementForUpperAndMainDeck
 * @property string $price
 *
 * @property Page $menu
 */
class Pricing extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pricing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'startDate', 'endDate'], 'required'],
            [['menu_id'], 'integer'],
            [['startDate', 'endDate', 'perPersonInTwin', 'netSingleSupplement', 'supplementForUpperAndMainDeck', 'price'], 'string'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
            'perPersonInTwin' => 'Per Person In Twin',
            'netSingleSupplement' => 'Net Single Supplement',
            'supplementForUpperAndMainDeck' => 'Supplement For Upper And Main Deck',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'menu_id']);
    }
}
