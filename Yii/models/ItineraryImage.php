<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "itinerary_image".
 *
 * @property integer $id
 * @property integer $itinerary_id
 * @property string $image
 * @property string $caption
 *
 * @property Itinerary $itinerary
 */
class ItineraryImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'itinerary_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['itinerary_id', 'image', 'caption'], 'required'],
            [['itinerary_id'], 'integer'],
            [['image', 'caption'], 'string'],
            [['itinerary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Itinerary::className(), 'targetAttribute' => ['itinerary_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'itinerary_id' => 'Itinerary ID',
            'image' => 'Image',
            'caption' => 'Caption',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItinerary()
    {
        return $this->hasOne(Itinerary::className(), ['id' => 'itinerary_id']);
    }
}
