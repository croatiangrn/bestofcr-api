<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slideshow".
 *
 * @property integer $id
 * @property string $photo
 * @property string $caption
 * @property integer $position
 */
class Slideshow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slideshow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['photo', 'caption', 'position'], 'required'],
            [['caption'], 'string'],
            [['position'], 'integer'],
            [['photo'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photo' => 'Photo',
            'caption' => 'Caption',
            'position' => 'Position',
        ];
    }
}
