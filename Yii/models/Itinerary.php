<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "itinerary".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property string $title
 * @property string $description
 *
 * @property Menu $menu
 * @property ItineraryImage[] $itineraryImages
 */
class Itinerary extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'itinerary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'title', 'description'], 'required'],
            [['menu_id'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 500],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Page::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItineraryImages()
    {
        return $this->hasMany(ItineraryImage::className(), ['itinerary_id' => 'id']);
    }

    public function fields() {
        $fields = parent::fields();
        $fields['itineraryImages'] = 'itineraryImages';
        return $fields;
    }
}
