<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 11.01.17.
 * Time: 13:07
 */

namespace app\models;


use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord {
    public function saveAjax($parsedData = null) {
        if ($parsedData === null) {
            $data = json_decode(file_get_contents('php://input'), true);
        } else {
            $data = $parsedData;
        }
        $attributes = array_flip($this->attributes());
        foreach ($data as $name => $value) {
            if (isset($attributes[$name])) {
                $this->$name = $value;
            }
        }
        return $this->save();
    }
}