<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title>Hello World</title>
</head>
<body>
<?php
$this->beginBody();
echo $content;
$this->endBody();
?>
</body>
</html>
<?php $this->endPage() ?>
