<?php

namespace app\components;

use app\models\Page;

class DBMenuHelper
{
    /**
     * Used for backend
     * @var array
     */
    protected $rezultat = [];


    public static function getMenu()
    {
        $role_id = 0;
        $result = static::getMenuRecrusive($role_id);
        return $result;
    }

    private static function getMenuRecrusive($parent)
    {

        $items = Page::find()
            ->where(['parent' => $parent])
            ->orderBy('id ASC')
            ->asArray()
            ->all();

        $result = [];

        foreach ($items as $item) {
            $postojiChild = Page::find()->where('parent = :id',[':id' => $item['id']])->count();
            $broj_crtica = str_repeat('-', $postojiChild);
            $result[] = [
                'label' => $item['title'],
                'url' => ['site/kategorija','slug' => $item['slug']],
                'items' => static::getMenuRecrusive($item['id']),
                'id' => $item['id'],
            ];
        }
        return $result;
    }

    /**
     * Check if there's child elements in element
     * @param $rows
     * @param $id
     * @return bool
     */
    protected function has_children($rows, $id) {
        foreach ($rows as $row) {
            if ($row->parent == $id)
                return true;
        }
        return false;
    }

    /**
     * Will be shown in app backend
     * @param $rows
     * @param int $parent
     * @param int $depth
     * @return mixed
     */
    public function buildMenu($rows,$parent=0, $depth = 0) {
        foreach ($rows as $row) {
            if ($row->parent == $parent){
                if ($depth == 0) {
                    $row->title = str_repeat('-',$depth).''.$row->title;
                } else {
                    $row->title = str_repeat('-',$depth).' '.$row->title;
                }
                array_push($this->rezultat,$row);
                if ($this->has_children($rows,$row->id)) {
                    $this->buildMenu($rows,$row->id, $depth+1);
                }
            }
        }
        return $this->rezultat;
    }

}
