<?php

namespace app\components;

use app\models\Page;

class MenuHelper
{
    public function menuItems($parent_id = 0)
    {
        $result = static::getMenuRecursive($parent_id);
        return $result;
    }

    private static function getMenuRecursive($parent)
    {
        $items = Page::find()
            ->where(['parent' => $parent])
            ->orderBy('id ASC')
            ->asArray()
            ->all();

        /**
         * Sort items by `position` field
         */
        uasort($items, function($a, $b) {
            return $a['position'] - $b['position'];
        });

        $result = [];


        foreach ($items as $item) {
            $result[] = [
                'id' 		=> $item['id'],
                'title' 	=> $item['title'],
                'slug' 		=> $item['slug'],
                'image' 	=> $item['image'],
                'col_md' 	=> $item['col_md'],
                'col_lg' 	=> $item['col_lg'],
                'img_class' => ($item['col_md'] == 17) ? 154 : ($item['col_md'] == 15) ? 204 : 254,
                'position' 	=> $item['position'],
                'parent' 	=> $item['parent'],
                'in_new_tab'=> $item['in_new_tab'],
                'subItems' 	=> static::getMenuRecursive($item['id']),
            ];
        }
        return $result;
    }
}