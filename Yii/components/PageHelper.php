<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 07.01.17.
 * Time: 15:27
 */

namespace app\components;


use app\models\Itinerary;
use app\models\Menu;
use app\models\Page;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class PageHelper
{
//    CREATE SECTION

    public function create() {

    }
//    CREATE SECTION END


//    UPDATE SECTION

    /**
     * This method is used to control update process.
     * @param array $data
     * @return object|array|string
     * @throws 404 error page
     */
    public function update($data) {
        switch($data->pageType){
            case 0:
                return $this->updatePageTypeZero($data);
                break;
            case 1:
                return $this->updatePageTypeOne($data);
                break;
            case 2:
                return $this->updatePageTypeTwo($data);
                break;
            default:
                throw new NotFoundHttpException();
        }
    }

    /**
     * Updates page with pageType 0
     * @param $data
     * @return bool
     */
    protected function updatePageTypeZero($data){
        return true;
    }

    /**
     * Updates page with pageType 1
     * @param $data
     * @return bool
     */
    protected function updatePageTypeOne($data){
        return true;
    }

    /**
     * Updates page with pageType 2
     * @param array $data
     * @return bool|array
     * @throws 400
     */
    protected function updatePageTypeTwo($data){
        $model = Page::find()->with('itineraries')->where('id = :id',[':id' => $data->id])->one();
        $model->pageType = $data->pageType;
        $model->update(false);
        for ($i = 0; $i < count($data->itineraries); $i++) {
            // Key exists - UPDATE Scenario for itinerary
            if (key_exists('id',$data->itineraries[$i])){
                $iModel = Itinerary::findOne($model->itineraries[$i]->id);
                $iModel->title = $data->itineraries[$i]->title;
                $iModel->description = $data->itineraries[$i]->description;
                $iModel->update(false);
                if (count($iModel->errors) > 0) {
                    throw new BadRequestHttpException();
                }
            } else { // Key doesn't exist - CREATE Scenario for itinerary
                $iModel = new Itinerary();
                $iModel->title = $data->itineraries[$i]->title;
                $iModel->menu_id = $data->itineraries[$i]->menu_id;
                $iModel->description = $data->itineraries[$i]->description;
                $iModel->save(false);
                if (count($iModel->errors) > 0) {
                    throw new BadRequestHttpException();
                }
            }
        }
        return $model;
    }

//    UPDATE SECTION END

//    DELETE SECTION
    public function delete() {

    }
//    DELETE SECTION END
}