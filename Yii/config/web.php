<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'iyclHs5ic2LCenj5R6x38F_tONKT1_1i',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableSession' => false,
            'loginUrl' => null
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'user',
                    'extraPatterns' => [
                        'POST login' => 'login',
                    ]

                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'page',
                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                        '{slug}' => '<slug>'
                    ],
                    'extraPatterns' => [
                        'GET getMenuByParent' => 'getmenubyparent',
                        'GET getMenuByPosition' => 'getmenubyposition',
                        'GET getmenubyslug/{slug}' => 'getmenubyslug'
                    ]

                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'itinerary',
                    'extraPatterns' => [
//                        'GET getMenuByParent' => 'getmenubyparent',
                    ]

                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'slideshow',
                    'extraPatterns' => [
//                        'GET getMenuByParent' => 'getmenubyparent',
                    ]

                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'itinerary-image',
                    'extraPatterns' => [
//                        'GET getMenuByParent' => 'getmenubyparent',
                    ]

                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'yacht-specification',
                    'extraPatterns' => [
                        'GET getByMenuId/{id}' => 'getbymenuid'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'yacht-pricing',
                    'extraPatterns' => [
//                        'GET getByMenuId/{id}' => 'getbymenuid'
                    ]
                ],
            ],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
