<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 07.01.17.
 * Time: 15:16
 */

namespace app\controllers;

use app\components\DBMenuHelper;
use app\components\MenuHelper;
use app\models\Itinerary;
use app\models\Page;
use app\models\Pricing;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class PageController extends ActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 86400,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['options', 'index', 'view', 'getmenubyparent', 'getmenubyslug', 'getmenubyposition']
        ];

        return $behaviors;
    }

    public $modelClass = 'app\models\Page';

    public function actions() {
        // Override default actions
        $actions = parent::actions();
        unset($actions['index'], $actions['create'], $actions['update']);
        return $actions;
    }

    /**
     * Renders all menu items
     * @return ActiveDataProvider
     */
    public function actionIndex() {
        $activeData = new ActiveDataProvider([
            'query' => Page::find()->with('itineraries'),
            'pagination' => false
        ]);
        return $activeData;
    }

    /**
     * Adds new menu item
     * @return array|string
     */
    public function actionCreate() {
        return $this->dbRow();
    }

    public function actionUpdate($id) {
        $data = json_decode(file_get_contents('php://input'), true);
        $model = Page::findOne($id);
        $model->parent = $data['parent'];
        if ($model->getPrevParentID($model->parent) === false) {
            throw new BadRequestHttpException();
        }
        if ($model->load($data, '') && $model->validate()) {
            $model->update();
        }
        $itineraries = $data['itineraries'];
        if ($itineraries) {
            for($i = 0; $i < count($itineraries); $i++) {
                if (!key_exists('id',$itineraries[$i])) {
                    $itinerary = new Itinerary();
                    $itinerary->menu_id = $id;
                    $itinerary->title = $itineraries[$i]['title'];
                    $itinerary->description = $itineraries[$i]['description'];
                    if (($itinerary->validate() && $itinerary->save()) === false) {
                        throw new BadRequestHttpException();
                    }
                } else {
                    $itinerary = Itinerary::findOne($itineraries[$i]['id']);
                    if (!$itinerary->saveAjax($itineraries[$i])) {
                        throw new BadRequestHttpException();
                    }
                }
            };
        }
        return $model;
    }

    public function actionGetmenubyparent() {
        $rows = json_decode(file_get_contents(Url::to(['page/index'], true)));
        $dbMenu = new DBMenuHelper();
        return $dbMenu->buildMenu($rows);
    }

    public function actionGetmenubyposition(){
        $menuHelper = new MenuHelper();
        return $menuHelper->menuItems();
    }

    public function actionGetmenubyslug($slug) {
        $model = Page::find()->where('slug = :slug',[':slug' => $slug])->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        return $model;
    }

    /**
     * /
     * @param bool $create Whether the row is being inserted or updated
     * @return array|string
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    protected function dbRow($create = true) {
        $data = json_decode(file_get_contents('php://input'));
        if ($create) {
            $model = new $this->modelClass;
        } else {
            $model = Page::findOne($data->id);
        }
        $model->title = $data->title;
        $model->parent = $data->parent;
        $model->position = $model->calculatePosition($model->parent);
        if (!$model->getPrevParentID($model->parent)) {
            throw new NotFoundHttpException();
        }
        $model->image = 'default.jpg';
        $model->hotels = 1;
        $model->itinerary = 1;

        if ($create) {
            if ($model->validate() && $model->save(false)) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(201);
                $id = implode(',', array_values($model->getPrimaryKey(true)));
                $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $id], true));
            } else {
                return $model->errors;
            }
        } else {
            if ($model->save() === false && !$model->hasErrors()) {
                throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
            }
            return $model;
        }
        return $model->errors;

    }
}