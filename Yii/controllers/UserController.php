<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 16.01.17.
 * Time: 13:14
 */

namespace app\controllers;

use Yii;
use app\models\User;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;

class UserController extends ActiveController {
    public $modelClass = 'app\models\User';


    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['login']
        ];

        return $behaviors;
    }

    public function actionLogin() {
        $data = json_decode(file_get_contents('php://input'), true);
        $model = User::find()->where('username = :user',[':user' => $data['username']])->one();
        if ($model === null) {
            throw new BadRequestHttpException();
        }
        if (!Yii::$app->security->validatePassword($data['password'], $model->password_hash)) {
            throw new BadRequestHttpException();
        }
        return $model;
    }
}