<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        Yii::$app->response->headers->add('Content-Type', 'application/json');
        Yii::$app->response->format = Response::FORMAT_JSON;
        echo json_encode('Hello world');
        return;
    }
}
