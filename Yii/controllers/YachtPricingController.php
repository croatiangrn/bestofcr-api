<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 16.01.17.
 * Time: 15:01
 */

namespace app\controllers;


use app\models\Pricing;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;

class YachtPricingController extends ActiveController {
    public $modelClass = 'app\models\Pricing';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 86400,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['options', 'index', 'view']
        ];

        return $behaviors;
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create'], $actions['update']);
        return $actions;
    }

    public function actionCreate() {
        $model = new Pricing();
        if ($model->saveAjax()) {
            $response = \Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $id], true));
        }
    }

    public function actionUpdate($id) {
        $model = Pricing::findOne($id);
        if ($model->saveAjax()) {
            return $model;
        } else {
            throw new BadRequestHttpException();
        }
    }
}