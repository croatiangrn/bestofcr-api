<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 08.01.17.
 * Time: 12:31
 */

namespace app\controllers;


use yii\rest\ActiveController;

class ItineraryController extends ActiveController
{
    public $modelClass = 'app\models\Itinerary';
}