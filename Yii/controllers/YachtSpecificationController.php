<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 13.01.17.
 * Time: 12:24
 */

namespace app\controllers;


use app\models\YachtSpecifications;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

class YachtSpecificationController extends ActiveController {

    public $modelClass = 'app\models\YachtSpecifications';

    /**
     * @inheritdoc
     */
    public function actions() {
        $actions = parent::actions();
        unset($actions['create'], $actions['update']);
        return $actions;
    }

    public function actionCreate() {
        $model = new YachtSpecifications();
        if ($model->saveAjax()) {
            $response = \Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $id], true));
        }
        return $model->errors;
    }

    public function actionUpdate($id) {
        $model = YachtSpecifications::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        $model->saveAjax();
        return $model;
    }

    public function actionGetbymenuid($id) {
        $model = YachtSpecifications::find()->where('menu_id = :id', [':id' => $id])->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}